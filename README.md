# **_ CALENDARIO con ANOTACIONES _**

Partiendo de la base que tengo en programación 💻 ( **Html, CSS, JavaScript** ) he creado una pequeña prueba de lo que sería una agenda con operaciones sencillas ( _ver meses, ver días, poder ir atras o adelante en los meses, poner notas diarias y poder borrar las notas_ ).

---

✅ Para ello he creado un `index.html` lo más correcto posible, con una estructura sencilla. He creado algunas etiquetas `<meta/>` que considero que tienen relativa importancia ( _author, description, keywords ..._ ).

✅ El siguiente paso fue ponerse con el archivo `.js` que será el encargado de realizar todo lo necesario para lo que queremos realizar en nuestro Calendario 🗓📅✍🏼.

```
const calendar = document.getElementById('calendar'); // div calendar:
const weekdays = [
  'lunes',
  'martes',
  'miercoles',
  'jueves',
  'viernes',
  'sábado',
  'domingo',
];
```

✅ Y para finalizar, he realizado el diseño, gracias al documento `style.css` le doy una imagen visual a la creación, tratando de proporcionarle un aspecto interesante, que no dañe demasiado la vista 👀 . E incorporando un darkMode en el archivo `darkMode.js`.

![Pequeña Imagen de creación](https://media.giphy.com/media/3oEjI6hkw6nbYNQkz6/giphy.gif)

---

> " El tiempo no es importante. Sólo la vida es importante. " — El Quinto Elemento

---

### **Autor ✒️ :**

Todo el trabajo ha sido creado por :

- <span style="color:green">Marcos</span> : [LinkedIn](https://es.linkedin.com/in/marcos-v%C3%A1zquez-gonz%C3%A1lez-44379562?trk=people-guest_people_search-card)
  ➡️ Creador de la estructura base en `html`, `javascript` y de diseñar la parte visual de `CSS`.

---

## Imagen/Preview del diseño visual ✍🏼 :

![Pequeña Imagen del diseño](/Preview.jpg)

---

## Funcionalidades :

> - Dark Mode ☠️ : que nos permite poner un modo Oscuro en caso de que el lado luminoso nos moleste. </br>
> - Poner eventos 📅 en cualquier día. Y quedan guardados en el localstore. </br>
> - Borrar eventos ␡ ya existentes. </br>
> - Navegar por los meses anteriores ◀️ y siguientes ▶️ . </br>
> - Marcado del día actual ◻️. </br>

---

## Web de la Aplicación ⚙ :

- [Link](https://calendarionotas.vercel.app/)

---

FUTURAS MEJORAS ⏰ :

\*\*\* Pendiente añadir (imagen Link ) footer y darle un formato mejor.

\*\*\* Posibilidad hacerlo Responsive.
